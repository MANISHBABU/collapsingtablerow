//
//  RootViewController.m
//  CollapsingTableDemo
//
//  Created by EsenseMac-35 on 08/01/14.
//  Copyright (c) 2014 EsenseMac-35. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

int x=0;
int y=0;
int z=0;
int a=0;

@synthesize  row1,row2,row3,row4,imageRow1,imageRow2,imageRow3,imageRow4,contentViewRow1,contentViewRow2,contentViewRow3,contentViewRow4;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self setFrame];
    // Do any additional setup after loading the view from its nib.
   
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)setFrame{
    
    // Rows & Content view initial position
    
    // For Row I
    [row1 setFrame:CGRectMake(0, 45, 320, 40)];
    [contentViewRow1 setFrame:CGRectMake(0, 85, 320, 0)];
    
    
    // For Row II
    [row2 setFrame:CGRectMake(0, row1.frame.origin.y+row1.frame.size.height+contentViewRow1.frame.size.height, 320, 40)];
    
    [contentViewRow2 setFrame:CGRectMake(0,row1.frame.origin.y+row1.frame.size.height+contentViewRow1.frame.size.height+row2.frame.size.height,320,0)];
    
    
    //For Row III
    [row3 setFrame:CGRectMake(0, row1.frame.origin.y+row1.frame.size.height+ contentViewRow1.frame.size.height+row2.frame.size.height+contentViewRow2.frame.size.height, 320, 40)];
    
    [contentViewRow3 setFrame:CGRectMake(0,row1.frame.origin.y+row1.frame.size.height+contentViewRow1.frame.size.height+row2.frame.size.height+ contentViewRow2.frame.size.height+row3.frame.size.height,320,0)];

   
    //For Row IV
    [row4 setFrame:CGRectMake(0, row1.frame.origin.y+row1.frame.size.height+ contentViewRow1.frame.size.height+row2.frame.size.height+contentViewRow2.frame.size.height+row3.frame.size.height+contentViewRow3.frame.size.height,320, 40)];
  
    [contentViewRow4 setFrame:CGRectMake(0,row1.frame.origin.y+row1.frame.size.height+contentViewRow1.frame.size.height+row2.frame.size.height+ contentViewRow2.frame.size.height+row3.frame.size.height+contentViewRow3.frame.size.height+row4.frame.size.height,320,0)];

    
    //setting image of button indicator
    
    [imageRow1 setImage:[UIImage imageNamed:@"sideArrow.png"]];
    [imageRow2 setImage:[UIImage imageNamed:@"sideArrow.png"]];
    [imageRow3 setImage:[UIImage imageNamed:@"sideArrow.png"]];
    [imageRow4 setImage:[UIImage imageNamed:@"sideArrow.png"]];
   
    
}


- (IBAction)row1ButtonPressed:(id)sender {
    
    if (x==0) {
        
        // Animation
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        
        //Setting Button Indicator Image on Drop Down
        [imageRow1 setImage:[UIImage imageNamed:@"DownArrow.png"]];
        
        //Content view position
        [contentViewRow1 setFrame:CGRectMake(row1.frame.origin.x,row1.frame.origin.y+row1.frame.size.height,320,100)];
        
        [contentViewRow2 setFrame:CGRectMake(0,row1.frame.origin.y+row1.frame.size.height+contentViewRow1.frame.size.height+row2.frame.size.height,320,contentViewRow2.frame.size.height)];
        
        [contentViewRow3 setFrame:CGRectMake(0,row1.frame.origin.y+row1.frame.size.height+contentViewRow1.frame.size.height+row2.frame.size.height+ contentViewRow2.frame.size.height+row3.frame.size.height,320,contentViewRow3.frame.size.height)];
        
        [contentViewRow4 setFrame:CGRectMake(0,row1.frame.origin.y+row1.frame.size.height+contentViewRow1.frame.size.height+row2.frame.size.height+ contentViewRow2.frame.size.height+row3.frame.size.height+contentViewRow3.frame.size.height+row4.frame.size.height,320,contentViewRow4.frame.size.height)];
        
        
        // Rows Positioning
        [row2 setFrame:CGRectMake(0, row1.frame.origin.y+row1.frame.size.height+contentViewRow1.frame.size.height, 320, 40)];

        [row3 setFrame:CGRectMake(0, row1.frame.origin.y+row1.frame.size.height+ contentViewRow1.frame.size.height+row2.frame.size.height+contentViewRow2.frame.size.height, 320, 40)];

        [row4 setFrame:CGRectMake(0, row1.frame.origin.y+row1.frame.size.height+ contentViewRow1.frame.size.height+row2.frame.size.height+contentViewRow2.frame.size.height+row3.frame.size.height+contentViewRow3.frame.size.height,320, 40)];

        
        [UIView commitAnimations];
        
        x=1;
    }
    
    else {
        
        // Animation
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        
        // Set Button Indicator Image
        [imageRow1 setImage:[UIImage imageNamed:@"sideArrow.png"]];
        
        //Content view position
        [contentViewRow1 setFrame:CGRectMake(row1.frame.origin.x,row1.frame.origin.y+row1.frame.size.height,320,0)];
        
        [contentViewRow2 setFrame:CGRectMake(0,row1.frame.origin.y+row1.frame.size.height+contentViewRow1.frame.size.height+row2.frame.size.height,320,contentViewRow2.frame.size.height)];
        
        [contentViewRow3 setFrame:CGRectMake(0,row1.frame.origin.y+row1.frame.size.height+contentViewRow1.frame.size.height+row2.frame.size.height+ contentViewRow2.frame.size.height+row3.frame.size.height,320,contentViewRow3.frame.size.height)];
        
        [contentViewRow4 setFrame:CGRectMake(0,row1.frame.origin.y+row1.frame.size.height+contentViewRow1.frame.size.height+row2.frame.size.height+ contentViewRow2.frame.size.height+row3.frame.size.height+contentViewRow3.frame.size.height+row4.frame.size.height,320,contentViewRow4.frame.size.height)];
        
        // Rows Positioning
        [row1 setFrame:CGRectMake(0, 45, 320, 40)];
        
        [row2 setFrame:CGRectMake(0, row1.frame.origin.y+row1.frame.size.height+contentViewRow1.frame.size.height, 320, 40)];
        
        [row3 setFrame:CGRectMake(0, row1.frame.origin.y+row1.frame.size.height+ contentViewRow1.frame.size.height+row2.frame.size.height+contentViewRow2.frame.size.height, 320, 40)];
        
        [row4 setFrame:CGRectMake(0, row1.frame.origin.y+row1.frame.size.height+ contentViewRow1.frame.size.height+row2.frame.size.height+contentViewRow2.frame.size.height+row3.frame.size.height+contentViewRow3.frame.size.height,320, 40)];
        
       
        [UIView commitAnimations];

        x=0;
    }

}


- (IBAction)row2ButtonPressed:(id)sender {
    
    if (y==0) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        
        
        //Setting Button Indicator Image on Drop Down
        [imageRow2 setImage:[UIImage imageNamed:@"DownArrow.png"]];

        //Content view position
        [contentViewRow2 setFrame:CGRectMake(row2.frame.origin.x,row2.frame.origin.y+row2.frame.size.height,320,100)];

        [contentViewRow3 setFrame:CGRectMake(0,row1.frame.origin.y+row1.frame.size.height+contentViewRow1.frame.size.height+row2.frame.size.height+ contentViewRow2.frame.size.height+row3.frame.size.height,320,contentViewRow3.frame.size.height)];
        
        [contentViewRow4 setFrame:CGRectMake(0,row1.frame.origin.y+row1.frame.size.height+contentViewRow1.frame.size.height+row2.frame.size.height+ contentViewRow2.frame.size.height+row3.frame.size.height+contentViewRow3.frame.size.height+row4.frame.size.height,320,contentViewRow4.frame.size.height)];
        
        
        // Rows  positioning

        [row3 setFrame:CGRectMake(0, row1.frame.origin.y+row1.frame.size.height+ contentViewRow1.frame.size.height+row2.frame.size.height+contentViewRow2.frame.size.height, 320, 40)];
        
        [row4 setFrame:CGRectMake(0, row1.frame.origin.y+row1.frame.size.height+ contentViewRow1.frame.size.height+row2.frame.size.height+contentViewRow2.frame.size.height+row3.frame.size.height+contentViewRow3.frame.size.height,320, 40)];
        

        [UIView commitAnimations];
       
        y=1;
    }
    
    else {
        
        
        
        // Animation
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        
        //Setting Button Indicator Image on Drop Down
        [imageRow2 setImage:[UIImage imageNamed:@"SideArrow.png"]];
        
        //Content view position
        [contentViewRow2 setFrame:CGRectMake(row2.frame.origin.x,row2.frame.origin.y+row2.frame.size.height,320,0)];
   
        [contentViewRow3 setFrame:CGRectMake(0,row1.frame.origin.y+row1.frame.size.height+contentViewRow1.frame.size.height+row2.frame.size.height+ contentViewRow2.frame.size.height+row3.frame.size.height,320,contentViewRow3.frame.size.height)];
        
        [contentViewRow4 setFrame:CGRectMake(0,row1.frame.origin.y+row1.frame.size.height+contentViewRow1.frame.size.height+row2.frame.size.height+ contentViewRow2.frame.size.height+row3.frame.size.height+contentViewRow3.frame.size.height+row4.frame.size.height,320,contentViewRow4.frame.size.height)];

        
        //Rows Positioning
        [row3 setFrame:CGRectMake(0, row1.frame.origin.y+row1.frame.size.height+ contentViewRow1.frame.size.height+row2.frame.size.height+contentViewRow2.frame.size.height, 320, 40)];
        
        [row4 setFrame:CGRectMake(0, row1.frame.origin.y+row1.frame.size.height+ contentViewRow1.frame.size.height+row2.frame.size.height+contentViewRow2.frame.size.height+row3.frame.size.height+contentViewRow3.frame.size.height,320, 40)];
        
        [UIView commitAnimations];

        
        y=0;
    }

}


- (IBAction)row3ButtonPressed:(id)sender {
    
    
    if (z==0) {
        
        // Animation
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];

        
        //Setting Button Indicator Image on Drop Down
        [imageRow3 setImage:[UIImage imageNamed:@"DownArrow.png"]];
        
        //Content view position
        [contentViewRow3 setFrame:CGRectMake(row3.frame.origin.x,row3.frame.origin.y+row3.frame.size.height,320,100)];

       [contentViewRow4 setFrame:CGRectMake(0,row1.frame.origin.y+row1.frame.size.height+contentViewRow1.frame.size.height+row2.frame.size.height+ contentViewRow2.frame.size.height+row3.frame.size.height+contentViewRow3.frame.size.height+row4.frame.size.height,320,contentViewRow4.frame.size.height)];
        
        
        // Rows Postioning
        [row4 setFrame:CGRectMake(0, row1.frame.origin.y+row1.frame.size.height+ contentViewRow1.frame.size.height+row2.frame.size.height+contentViewRow2.frame.size.height+row3.frame.size.height+contentViewRow3.frame.size.height,320, 40)];
        

        [UIView commitAnimations];

        z=1;
    }
    
    else {
        
        // Animation
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        
        //Setting Button Indicator Image on Drop Down
        [imageRow3 setImage:[UIImage imageNamed:@"SideArrow.png"]];

        //Content view position
        [contentViewRow3 setFrame:CGRectMake(row3.frame.origin.x,row3.frame.origin.y+row3.frame.size.height,320,0)];

        [contentViewRow4 setFrame:CGRectMake(0,row1.frame.origin.y+row1.frame.size.height+contentViewRow1.frame.size.height+row2.frame.size.height+ contentViewRow2.frame.size.height+row3.frame.size.height+contentViewRow3.frame.size.height+row4.frame.size.height,320,contentViewRow4.frame.size.height)];
        
        
        // Rows Positioning
        [row4 setFrame:CGRectMake(0, row1.frame.origin.y+row1.frame.size.height+ contentViewRow1.frame.size.height+row2.frame.size.height+contentViewRow2.frame.size.height+row3.frame.size.height+contentViewRow3.frame.size.height,320, 40)];

        
        [UIView commitAnimations];

        z=0;
    }
    

}


- (IBAction)row4ButtonPressed:(id)sender {
    
    if (a==0) {
        
        
        // Animation
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        
        
        //Setting Button Indicator Image on Drop Down
        [imageRow4 setImage:[UIImage imageNamed:@"DownArrow.png"]];
        
        //Content view position
        [contentViewRow4 setFrame:CGRectMake(row4.frame.origin.x,row4.frame.origin.y+row4.frame.size.height,320,100)];

        [UIView commitAnimations];
        
        a=1;
    }
    
    else {
        
        // Animation
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];

        //Setting Button Indicator Image on Drop Down
        [imageRow4 setImage:[UIImage imageNamed:@"SideArrow.png"]];
        
        //Content view position
        [contentViewRow4 setFrame:CGRectMake(row4.frame.origin.x,row4.frame.origin.y+row4.frame.size.height,320,0)];

        [UIView commitAnimations];
        a=0;
    }
    

}
@end
