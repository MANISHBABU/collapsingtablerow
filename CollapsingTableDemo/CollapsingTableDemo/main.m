//
//  main.m
//  CollapsingTableDemo
//
//  Created by EsenseMac-35 on 08/01/14.
//  Copyright (c) 2014 EsenseMac-35. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
