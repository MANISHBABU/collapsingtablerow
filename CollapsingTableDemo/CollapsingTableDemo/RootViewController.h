//
//  RootViewController.h
//  CollapsingTableDemo
//
//  Created by EsenseMac-35 on 08/01/14.
//  Copyright (c) 2014 EsenseMac-35. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController

// row 1
@property (strong, nonatomic) IBOutlet UIView *row1;
@property (strong, nonatomic) IBOutlet UIImageView *imageRow1;
- (IBAction)row1ButtonPressed:(id)sender;


// row 2
@property (strong, nonatomic) IBOutlet UIView *row2;
@property (strong, nonatomic) IBOutlet UIImageView *imageRow2;
- (IBAction)row2ButtonPressed:(id)sender;

// row 3
@property (strong, nonatomic) IBOutlet UIView *row3;
@property (strong, nonatomic) IBOutlet UIImageView *imageRow3;
- (IBAction)row3ButtonPressed:(id)sender;

//row 4
@property (strong, nonatomic) IBOutlet UIView *row4;
@property (strong, nonatomic) IBOutlet UIImageView *imageRow4;
- (IBAction)row4ButtonPressed:(id)sender;


// content view for row 1
@property (strong, nonatomic) IBOutlet UIView *contentViewRow1;

// content view for row 2
@property (strong, nonatomic) IBOutlet UIView *contentViewRow2;

// content view for row 3
@property (strong, nonatomic) IBOutlet UIView *contentViewRow3;

// content view for row 4
@property (strong, nonatomic) IBOutlet UIView *contentViewRow4;
//@property (strong, nonatomic) IBOutlet UIView *tempView;

// Initializing Frames On view load
-(void)setFrame;
@end
