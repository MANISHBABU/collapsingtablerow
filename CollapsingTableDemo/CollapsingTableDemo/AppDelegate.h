//
//  AppDelegate.h
//  CollapsingTableDemo
//
//  Created by EsenseMac-35 on 08/01/14.
//  Copyright (c) 2014 EsenseMac-35. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
